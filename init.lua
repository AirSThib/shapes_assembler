minetest.register_node("shapes_assembler:assembler", {
	description = "Assembler",
	tiles = {"shapes_assembler_assembler.png"},
	groups = {cracky = 1},
	on_construct = function(pos, placer)
		local meta = minetest.get_meta(pos)
		local inv = meta:get_inventory()
		local moreblocks = minetest.get_modpath("moreblocks") ~= nil
		inv:set_size("main", 2)
		local formspec = "size[8,6]" ..
                                        "list[context;main;2.1,0.1;1,2;]" ..
                                        "label[3.1,0.2;Blue shape]" ..
                                        "label[3.1,1.2;Red shape]" ..
					"button[0.1,0.1;1.8,0.8;update;Update]"
		meta:from_table({
			inventory = {
				main = {[1] = "", [2] = ""}
			},
			
			fields = {
				formspec = formspec,
				infotext = "Assembler"
			}
		})

	end,
	on_receive_fields = function(pos, formname, fields, sender)
		if fields.update then
		end
	end
})

minetest.register_craft({
	output = "shapes_assembler:assembler",
	recipe = {
		{"default:wood", "screwdriver:screwdriver", "default:steel_ingot"}
	}
})
